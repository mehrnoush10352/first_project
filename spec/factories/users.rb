FactoryBot.define do
  factory :user do
    username "test" 
    password "Ab456456"
    role "admin"
    is_active true
    email "test@yahoo.com"
    trait :invalid_email do
      email "invalid_email"
    end
    trait :invalid_username do
      username "invalid username *&"
    end
    trait :invalid_status do
      is_active nil 
    end
    trait :invalid_password do
      password '12345678'
    end
  end
end
