require 'rails_helper'

context User, type: :model do
  it 'create valid user' do
    user = FactoryBot.build(:user)
    user.should be_valid
  end
  it 'invalid email' do
    user = FactoryBot.build(:user, :invalid_email)
    user.should be_invalid
  end
  it 'invalid username' do
    user = FactoryBot.build(:user, :invalid_username)
    user.should be_invalid
  end
  it 'invalid status' do
    user = FactoryBot.build(:user, :invalid_status)
    user.should be_invalid
  end
  it 'invalid password' do
    user = FactoryBot.build(:user, :invalid_password)
    user.should be_invalid
  end
end
