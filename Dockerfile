FROM ruby:2.4.1
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs
RUN mkdir /first_project
WORKDIR /first_project
ADD Gemfile /first_project/Gemfile
ADD Gemfile.lock /first_project/Gemfile.lock
RUN gem install bundler
RUN bundle install
Add . /first_project
