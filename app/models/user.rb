class User < ApplicationRecord
  validates_format_of :email, with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i
  validates_format_of :username, with: /\A[a-zA-Z0-9]+\Z/i
  validates :is_active, inclusion: { in: [true, false] }
  validates :password, {confirmation: true, presence: true, length: { minimum: 8 }, format: {
                          with: /\A(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+\Z/i,
                          message: "must contain 3 of the following 4: a lowercase letter, an uppercase letter, a digit 1"  }}
end
